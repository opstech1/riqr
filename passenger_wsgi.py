import sys, os
from dotenv import load_dotenv

load_dotenv()
# INTERP = os.path.join(os.environ['HOME'], 'dev.pdf.tradition1871.com', 'venv', 'bin', 'python3')
if os.environ["environment"] == "production":
    INTERP = os.environ["interpreter"]
elif os.environ["environment"] == "development":
    INTERP = os.environ["interpreter"]
    os.environ["DEBUG"] = "*"
else:
    print(
        "No environment system variable set. Must be set to production or development in the .env file."
    )
if sys.executable != INTERP:
    os.execl(INTERP, INTERP, *sys.argv)
sys.path.append(os.getcwd())

sys.path.append("app")
from app import app as application
