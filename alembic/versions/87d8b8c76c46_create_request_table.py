"""create request table

Revision ID: 87d8b8c76c46
Revises: 
Create Date: 2021-10-22 09:34:30.348775

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql.expression import false, null
from sqlalchemy.sql import func

# revision identifiers, used by Alembic.
revision = '87d8b8c76c46'
down_revision = None
branch_labels = None
depends_on = None

def upgrade():
    op.create_table(
        'request',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('event_id', sa.String(500), nullable=False, unique=True),
        sa.Column('email', sa.String(500), nullable=False),
        sa.Column('group', sa.String(500), nullable=True),
        sa.Column('sort', sa.String(500), nullable=True),
        sa.Column('created_at', sa.DateTime(timezone=True), server_default=func.now()),
    )

def downgrade():
    op.drop_table('request')
