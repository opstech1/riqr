"""create badge table

Revision ID: 04508d550270
Revises: 87d8b8c76c46
Create Date: 2021-10-22 13:12:40.751437

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import func


# revision identifiers, used by Alembic.
revision = '04508d550270'
down_revision = '87d8b8c76c46'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'badge',
        sa.Column('id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('event_id', sa.String(500), nullable=False),
        sa.Column('email', sa.String(500), nullable=False),
        sa.Column('image_data', sa.String(20000), nullable=False),
        sa.Column('name', sa.String(500), nullable=False),
        sa.Column('created_at', sa.DateTime(timezone=True), server_default=func.now()),
        sa.Column('sequence', sa.Integer())
    )


def downgrade():
    op.drop_table('badge')
