import http
from typing import Sequence
import requests
import json

from datetime import datetime
from datetime import timedelta
from app import app
from flask import request
from werkzeug.exceptions import HTTPException
from app.send_email import email_pdf
from app.avery import generate_avery_pdf
from app.db import get_db, Badge, Request
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
from markupsafe import escape

# Batch post an entire set of badges in one go
@app.route('/badges', methods=['POST'])
def badges():
    engine = get_db()
    params = request.get_json()
    email, badge_data = params['email'], params['badge_data']
    if email and email.endswith('@owensboro.kyschools.us'):
        output_file = generate_avery_pdf(data=badge_data)
        result = email_pdf(email, output_file)
    else:
        raise HTTPException("Invalid Parameters")
    return '', http.HTTPStatus.NO_CONTENT

# Commit a badge to the DB
@app.route('/badge', methods=['POST'])
def badge():
    params = request.get_json()
    email, event_id, badge_data = params['email'], params['event_id'], params['badge_data']
    if email and email.endswith('@owensboro.kyschools.us'):
        image_data = badge_data['image_data']
        name = badge_data['name']

        engine = get_db()
        Session = sessionmaker(bind=engine)
        session = Session()

        req = session.query(Request).join(Request.badges).filter_by(email=email).first()
        if(req):
            badge_count = len(req.badges) + 1
        else:
            req = Request(event_id=event_id, email=email)
            badge_count = 1
            
        badge = Badge(event_id=event_id, image_data=image_data, name=name, sequence=badge_count)
        req.badges.append(badge)
        session.add(req)
        session.commit()
        session.close()
    else:
        raise HTTPException("Invalid Parameters")
    return '', http.HTTPStatus.NO_CONTENT

# Trigger the email function for requests older than 60 seconds.
@app.route('/trigger')
def trigger():
    engine = get_db()

    Session = sessionmaker(bind=engine)
    session = Session()
    since = (datetime.now() - timedelta(minutes=1)).strftime("%Y/%m/%d %H:%M:%S")
    reqs = session.query(Request).join(Badge).order_by(Badge.sequence).filter(Request.created_at <= since ).all()
    if not reqs:
        session.close()
        return "Nothing to Do"
    payloads = []
    for req in reqs:
        payload = {
            "email": req.email,
            "badge_data": [],
            "event_id": req.event_id,
        }
        for badge in req.badges:
            payload['badge_data'].append( {
                "image_data": badge.image_data,
                "name": badge.name,
            } )
        payloads.append(payload)
    #results = []
    url = request.url_root + 'badges'
    for payload in payloads:
        result = requests.post(url, json=payload)
        if result.status_code not in [200, 204]:
            #results.append("Failure! " + json.dumps(payload['event_id']))
            output = "failure"
        else:
            #results.append("Success! " + json.dumps(payload['event_id']))
            session.delete(req)
            session.commit()
            output = "success"
    #return json.dumps(results)
    session.close()
    return output

        