import smtplib
import configparser
from datetime import datetime
from email.message import EmailMessage

def email_pdf(email: str, file):
    # Get Config Information
    config = configparser.ConfigParser()
    config.read('app/app.ini')
    
    msg = EmailMessage()
    #my_date = datetime.now().strftime("%m/%d/%Y %-I:%M:%S %p")
    my_date = datetime.utcnow().isoformat(' ')
    msg['Subject'] = 'RapidIdentity QR Codes. (ID:' + my_date + ')'
    
    msg['From'] = config['email']['From']
    msg['To'] = email
    msg.set_content("See Attached")
    ctype = 'application/octet-stream'
    maintype, subtype = ctype.split('/', 1)
    
    try:
        if file:
            msg.add_attachment(file.read(), maintype=maintype, subtype=subtype, filename='badges.pdf')
        s = smtplib.SMTP(config['email']['Server'], config['email']['Port'])
        if config['email'].getboolean('UseTLS'):
            s.starttls()    
        s.login(config['email']['User'], config['email']['Password'])
        s.send_message(msg)
        s.quit()
        return True
    except smtplib.SMTPException:
        return False
