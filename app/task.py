import requests
import configparser


def trigger():
    config = configparser.ConfigParser()
    config.read('app/app.ini')
    endpoint_config = config['endpoint']
    requests.get(endpoint_config['url'] + "/trigger")

if __name__ == "__main__":
    trigger()