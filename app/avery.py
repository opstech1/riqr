import labels
from reportlab.graphics import shapes
from PIL import Image
from io import BytesIO
from base64 import b64decode
from reportlab.pdfbase.pdfmetrics import stringWidth


def generate_avery_pdf(type: str = "22806", data: dict = {}):
    # This file is part of pylabels, a Python library to create PDFs for printing
    # labels.
    # Copyright (C) 2012, 2013, 2014 Blair Bonnett
    #
    # pylabels is free software: you can redistribute it and/or modify it under the
    # terms of the GNU General Public License as published by the Free Software
    # Foundation, either version 3 of the License, or (at your option) any later
    # version.
    #
    # pylabels is distributed in the hope that it will be useful, but WITHOUT ANY
    # WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
    # A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
    #
    # You should have received a copy of the GNU General Public License along with
    # pylabels.  If not, see <http://www.gnu.org/licenses/>.

    avery_labels = {
        "22806": {
            "sheet_width": 215.9,
            "sheet_height": 279.4,
            "columns": 3,
            "rows": 4,
            "label_width": 50.8,
            "label_height": 50.8,
            "left_margin": 15.875,
            "top_margin": 15.875,
            "bottom_margin": 15.875,
            "right_margin": 15.875,
            "column_gap": 15.875,
            "row_gap": 14.816666666666666,
            "corner_radius": 0,
        }
    }
    specs = labels.Specification(**avery_labels[type])
    # Create a function to draw each label. This will be given the ReportLab drawing
    # object to draw on, the dimensions (NB. these will be in points, the unit
    # ReportLab uses) of the label, and the object to render.
    def draw_label(label, width, height, obj, font: str = "Helvetica"):
        # Create a byte stream of the image_data and open it like a file
        image_file = BytesIO(b64decode(obj["image_data"]))
        img = Image.open(image_file)

        # Add the filelike image to the label
        label.add(shapes.Image(10, 25, 125, 125, img))

        # Format the name in the label
        name = obj["name"]
        font_size = 20
        text_width = width - 10
        name_width = stringWidth(name, font, font_size)
        while name_width > text_width:
            font_size *= 0.8
            name_width = stringWidth(name, font, font_size)
        label.add(
            shapes.String(
                width / 2.0,
                10,
                str(name),
                fontName=font,
                fontSize=font_size,
                textAnchor="middle",
            )
        )

    # Create the sheet.
    sheet = labels.Sheet(
        specs,
        draw_label,
        border=False,
    )

    # Add the labels
    sheet.add_labels(data)

    # Create a temporary in-memory file-like object and save to it
    tmp = BytesIO()
    sheet.save(tmp)
    # Rewind to the beginning
    tmp.seek(0)

    # print("{0:d} label(s) output on {1:d} page(s).".format(sheet.label_count, sheet.page_count))
    # Return the temp object
    return tmp
