import configparser
from sqlalchemy import (
    create_engine,
    Column,
    ForeignKey,
    Integer,
    String,
    Unicode,
    DateTime,
)
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relation, relationship
from sqlalchemy.sql.schema import FetchedValue, UniqueConstraint

Base = declarative_base()


def get_db() -> Engine:
    # Get Config Information
    config = configparser.ConfigParser()
    config.read("app/app.ini")
    db_config = config["database"]
    connection_string = (
        db_config["dialect"]
        + ("+" + db_config["driver"] if db_config["driver"] else "")
        + "://"
        + db_config["username"]
        + ":"
        + db_config["password"]
        + "@"
        + db_config["host"]
        + "/"
        + db_config["dbname"]
    )
    return create_engine(connection_string, echo=True, future=True)


class Request(Base):
    __tablename__ = "request"
    id = Column(Integer, primary_key=True)
    event_id = Column(String, unique=True)
    email = Column(String)
    group = Column(String)
    sort = Column(String)
    created_at = Column(DateTime, FetchedValue())
    badges = relationship(
        "Badge", back_populates="request", passive_deletes=False, cascade="all, delete"
    )


class Badge(Base):
    __tablename__ = "badge"
    id = Column(Integer, primary_key=True)
    event_id = Column(String, unique=True)
    email = Column(String, ForeignKey("request.email", ondelete="CASCADE"))
    image_data = Column(Unicode(1000))
    name = Column(String(200))
    created_at = Column(DateTime, FetchedValue())
    request = relationship("Request", back_populates="badges")
    sequence = Column(Integer())
